package fr.ajc.formation.java;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CalculateurFactorielleTest {

	@Test
	void ResultatFactorielleDe4Est24() {
		assertEquals(24, CalculateurFactorielle.factorielle(4));
	}
	
	@Test
	void ResultatFactorielleDe3Est6() {
		assertEquals(6, CalculateurFactorielle.factorielle(3));
	}
	
	@Test
	void ResultatFactorielleDe6Est720() {
		assertEquals(720, CalculateurFactorielle.factorielle(6));
	}
	
	
	
}
